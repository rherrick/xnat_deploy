# XNAT Deployment Configuration Base Build #

This is the XNAT deployment configuration base build. This is not meant to be used 
directly, but instead be referenced by deployment configurations for specific instances.
This allows all derived configurations to use the same properties to actually deploy
configuration files, plugin jars, and applications.

## Referencing the Base Build Configuration ##

Add the following line to your **build.gradle** file:

```
apply from: "https://bitbucket.org/rherrick/xnat_deploy/src/master/deploy.gradle"
```

This should be _almost_ the first thing in your **build.gradle**, but must come after
specifying the **plugins** block:

```
plugins {
    id "application"
}

apply from: "https://bitbucket.org/rherrick/xnat_deploy/src/master/deploy.gradle"
```

This is a requirement for Gradle itself. A future release should allow configuring
the **plugins** block in the referenced configuration, but for now this is necessary.

## Deploying The Build ##

You can copy configuration files and download plugin jars and the XNAT application by
running:

```
$ ./gradlew
```

This copies all configuration files, plugin jars, and the XNAT war file to the folder
**build/deploy** in the same folder as your deployment configuration. You can specify 
another destination for plugins and configuration files by specifying the **xnatHome**
property when you run the deployment:


```
$ ./gradlew -PxnatHome=/data/xnat/home
```

This will copy all configuration files in the directory **src/main/config** to the folder
**${xnatHome}/config**, plugin jars to **${xnatHome}/plugins**, and the XNAT war file to
**${xnatHome}/webapps/ROOT.war** (note the renaming of the war file).

You can specify the exact location for configuration files and plugin jars separately
with the **configFolder** and **pluginFolder** properties:

```
$ ./gradlew -PconfigFolder=/data/xnat/home/config -PpluginFolder=/data/xnat/home/plugins
```

You can specify the folder and name for the XNAT war file with the properties
**warFolder** and **warName**:


```
$ ./gradlew -PwarFolder=/var/lib/tomcat7/webapps -PwarName=xnat.war
```

Finally you can specify the folder and name for the XNAT war file with a single property
**warFile**:

```
$ ./gradlew -PwarFile=/var/lib/tomcat7/webapps/xnat.war
```

Note that **warName** defaults to **ROOT.war**, so you can send the XNAT war file to
the Tomcat application folder as the root application with just the **warFolder**
property:

```
$ ./gradlew -PwarFolder=/var/lib/tomcat7/webapps
```

